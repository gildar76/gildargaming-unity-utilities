# README #

### The Great Push Test! ###

### GildarGaming Unity Utility classes ###

This repository contains some helper classes I've created over time. Most of them should work independently, however some classes depends on things in the base Utils library. If a class depends on a Unity package, it will be mentioned below in the description.

### Setup and Usage ###

Simply copy the folders you want to use into your project. Always include the classes in the base folder unless you know for sure you won't need them. Have a look in the DemoScripts folder for hints on how to use these classes.



### Contribution ###

Feel free to make pull requests to add stuff to these classes or create new ones. If you build new functionality, please put it in a new namespace below the GildarGaming.Utils namespace, and place the scripts in a folder by the same name.

### Contributors ###

### License ###

MIT (Free to use however you want)

## Libraries and functions

### Helper.cs

A static class that contains some helper functions. SHould be self documenting.

### Singleton.cs

A base singleton class to derive from when creating singletons and manager classes.

To create a singleton simply derive from this class like this:

`public class MyClass : Singleton<MyClass>`

### Pooling

This folder contains a few classes related to object pooling. You can create pools of different types. (Default will be added). Currently List, Queue and Stack is supported. NOTE: The ListPool is very unoptimized.

See DemoSpawnManager.cs in the DemoScripts folder for more information.

### Scheduling

A very simple action scheduler.

### UI

Some UI utility classes. Currently not much to see here.



