﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GildarGaming.Utils.FSM
{
    /// <summary>
    /// Default State class. Currently inherits from MonoBehaviour. Might make a version that doesn't
    /// </summary>
    [RequireComponent(typeof(GildarGaming.FSM.StateMachine))]
    public abstract class State : MonoBehaviour
    {
        public StateMachine Machine { get; set; }
        bool isInitializedd = false;
        public static implicit operator bool(State state)
        {
            return state != null;
        }
        /// <summary>
        /// initialization. If this wasn't a MonoBehaviour we could use the constructor to do this stuff. Every state needs access to the StateMachine.
        /// </summary>
        /// <param name="machine"></param>
        public void Initialize(StateMachine machine)
        {

            Machine = machine;
            OnStateInitialize(machine);
        }
        /// <summary>
        /// Initialization
        /// </summary>
        /// <param name="machine"></param>
        protected virtual void OnStateInitialize(StateMachine machine = null)
        {
        }

        public void StateEnter()
        {
            //IN case the state has not been initialized for some reason, do it here.
            if (!isInitializedd)
            {
                this.Initialize(GetComponent<StateMachine>());
                isInitializedd = true;
            }
            //Debug.Log("Entering " + this.GetType().ToString());
            enabled = true;
            OnStateEnter();
        }

        protected virtual void OnStateEnter()
        {
        }

        public void StateExit()
        {
            //Debug.Log("Exiting state ");
            OnStateExit();
            enabled = false;
        }

        protected virtual void OnStateExit()
        {
        }

        public void OnEnable()
        {
            if (!isInitializedd)
            {
                this.Initialize(GetComponent<StateMachine>());
                isInitializedd = true;
            }

            if (this != Machine.GetCurrentState)
            {
                enabled = false;
                Debug.LogWarning("Do not enable States directly. Use StateMachine.SetState");
            }
        }

        public void OnDisable()
        {
            if (this == Machine.GetCurrentState)
            {
                enabled = true;
                Debug.LogWarning("Do not disable States directly. Use StateMachine.SetState");
            }
        }
    }

}
