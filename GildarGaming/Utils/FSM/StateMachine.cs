using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GildarGaming.Utils.FSM
{
    /// <summary>
    /// Inspired by some code from https://noahbannister.blog/2017/12/19/building-a-modular-character-controller/
    /// </summary>
	public class StateMachine : MonoBehaviour
	{
        public List<State> statesList = new List<State>();
        public  State startingState;
        protected State currentState;
        protected State previousState;

        /// <summary>
        /// Initialize the machine with the states on the gameobject and set the current state.
        /// </summary>
        public void Start()
        {
            //Get all State components on the objects and add them to the list. otherwise they'll never be initialized.
            statesList.AddRange(GetComponents<State>());
            foreach (State state in statesList)
            {
                Debug.Log("Starting");
                state.Initialize(this);
                
            }
            SetState(startingState);


        }

        public State GetCurrentState { get { return currentState; } }

        /// <summary>
        /// Switch the currentState to a specific State object
        /// </summary>
        /// <param name="state">
        /// The state object to set as the currentState</param>
        /// <returns>Whether the state was changed</returns>
        public virtual bool SetState(State state)
        {
            bool success = false;
            if (state && state != currentState)
            {
                previousState = currentState;
                currentState = state;
                if (previousState)
                {
                    previousState.StateExit();
                }

                currentState.StateEnter();
                success = true;
            }
            return success;
        }

        //public void SetState(State state)
        //{
        //    SetState(state);
        //}

        /// <summary>
        /// Switch the currentState to a State of a the given type.
        /// </summary>
        /// <typeparam name="StateType">
        /// The type of state to use for the currentState</typeparam>
        /// <returns>Whether the state was changed</returns>
        public virtual bool SetState<StateType>() where StateType : State
        {
            bool success = false;
            //if the state can be found in the list of states 
            //already created, switch to the existing version
            foreach (State state in statesList)
            {
                if (state is StateType)
                {
                    success = SetState(state);
                    return success;
                }
            }
            //if the state is not found in the list,
            //see if it is on the gameobject.
            State stateComponent = GetComponent<StateType>();
            if (stateComponent)
            {
                stateComponent.Initialize(this);
                statesList.Add(stateComponent);
                success = SetState(stateComponent);
                return success;
            }
            //if it is not on the gameobject,
            //make a new instance. NOTE: This sort of breaks the pattern. You may or may not wnat to add states at runtime.
            State newState = gameObject.AddComponent<StateType>();
            newState.Initialize(this);
            statesList.Add(newState);
            success = SetState(newState);

            return success;
        }
    }
}
