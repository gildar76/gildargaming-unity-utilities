using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GildarGaming.Utils
{
    /// <summary>
    /// COntains a few static helper functions.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Finds the closest GameObject from an array of objects
        /// </summary>
        /// <param name="currentPosition">The start position to use</param>
        /// <param name="objectArray">Array of gameobjects to search</param>
        /// <param name="maxDistance">Maxdistance from start position</param>
        /// <returns>The closest Gameobject to the start position</returns>
        public static GameObject FindClosestGameObject(Vector3 currentPosition, GameObject[] objectArray, float maxDistance = float.MaxValue)
        {
            float closestDistance = float.MaxValue;
            GameObject closest = null;
            for (int i = 0; i < objectArray.Length; i++)
            {
                if (objectArray[i] == null) continue;
                float currentDistance = Vector3.Distance(currentPosition, objectArray[i].transform.position);
                if (currentDistance < maxDistance && currentDistance < closestDistance)
                {
                    closestDistance = currentDistance;
                    closest = objectArray[i];
                }
            }
            return closest;
        }
        /// <summary>
        /// Find the closest GameObject froma list of objects
        /// </summary>
        /// <param name="currentPosition">The start position to use</param>
        /// <param name="objectArray">Array of gameobjects to search</param>
        /// <param name="maxDistance">Maxdistance from start position</param>
        /// <returns>The closest Gameobject to the start position</returns>
        public static GameObject FindClosestGameObject(Vector3 currentPosition, List<GameObject> ObjectList, float maxDistance = float.MaxValue)
        {
            float closestDistance = float.MaxValue;
            GameObject closest = null;
            for (int i = 0; i < ObjectList.Count; i++)
            {
                float currentDistance = Vector3.Distance(currentPosition, ObjectList[i].transform.position);
                if (currentDistance < maxDistance && currentDistance < closestDistance)
                {
                    closestDistance = currentDistance;
                    closest = ObjectList[i];
                }
            }
            return closest;
        }

        /// <summary>
        /// Checks if a layer is in a layermask. 
        /// </summary>
        /// <param name="mask"></param>
        /// <param name="layer"></param>
        /// <returns></returns>
        public static bool Contains(this LayerMask mask, int layer)
        {
            return mask == (mask | (1 << layer));
        }
        /// <summary>
        /// Removes a layer from a mask.
        /// </summary>
        /// <param name="mask"></param>
        /// <param name="layer"></param>
        /// <returns></returns>
        public static LayerMask RemoveFromMask(this LayerMask mask, int layer)
        {
            LayerMask newMask = mask & ~(1 << layer);
            return newMask;
        }

        /// <summary>
        /// Finds all components of the specified type in objects in the active scene 
        /// </summary>
        /// <typeparam name="T">The type to search for</typeparam>
        /// <param name="includeInactive">Search inactive objects</param>
        /// <returns></returns>
        public static List<T> FindAllComponentsOfType<T>(bool includeInactive)
        {
            GameObject[] rootGameobjects = SceneManager.GetActiveScene().GetRootGameObjects();
            List<T> componentList = new List<T>();
            foreach (var go in rootGameobjects)
            {
                
                T[] components = go.GetComponentsInChildren<T>(includeInactive);
                foreach (var item in components)
                {
                    componentList.Add(item);
                }
            }
            return componentList;

        }
    }
}
