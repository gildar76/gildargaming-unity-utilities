using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using GildarGaming.TowerOffense.Control;
using GildarGaming.TowerOffense.UI;

namespace GildarGaming.Utils.UI
{
    /// <summary>
    /// Class to handle clicks on any GUI object
    /// Inspired by this post: https://forum.unity.com/threads/can-the-ui-buttons-detect-a-right-mouse-click.279027/
    /// Assign a unity event to each mouse button.
    /// </summary>
    public class GUIClickController : MonoBehaviour, IRayCastable
    {
        public UnityEvent onLeftClick;
        public UnityEvent onRightClick;
        public UnityEvent onMiddleClick;

        public CursorType GetCursorType()
        {
            return CursorType.Valid;
        }

        public bool HandleRaycast()
        {
            return true;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                onLeftClick.Invoke();
            }
            else if (eventData.button == PointerEventData.InputButton.Right)
            {
                onRightClick.Invoke();
            }
            else if (eventData.button == PointerEventData.InputButton.Middle)
            {
                onMiddleClick.Invoke();
            }
        }
    }
}
