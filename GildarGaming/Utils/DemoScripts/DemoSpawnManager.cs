﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GildarGaming.Utils.Pooling;

namespace GildarGaming.Utils.Demo
{
    /// <summary>
    /// A demonstration class to show how to use the pool manager and the singleton classes.
    /// </summary>
    public class DemoSpawnManager : Singleton<DemoSpawnManager>
    {
        [SerializeField]
        GameObject objectToPool;
        float spawnTimer;
        float timeSinceLastSpawn = 0.1f;
        int spawnCounter = 0;
        List<GameObject> spawnedObjects; //Just a simple list to keep track of what we have spawned to simplify despawning from this script
        void Start()
        {
            
            //You can mix and match pooltypes.
            PoolManager.Instance.CreatePool(objectToPool, 500,PoolType.StackPool, null);
            //This list will  hold our spawned objects to allow despawning.
            //May not be needed depending on how you handle despawning.
            spawnedObjects = new List<GameObject>();
            
        }

        void Update()
        {
            spawnTimer += Time.deltaTime;
            if (spawnTimer >= timeSinceLastSpawn)
            {
                if (spawnCounter < PoolManager.Instance.GetPoolSize(objectToPool))
                {
                    spawnCounter++;
                    Debug.Log("Spawning object from pool");
                    //Generate a random position for spawning
                    Vector3 pos = new Vector3(Random.Range(-100f, 100f), Random.Range(-20f, 20f), 0f);
                    spawnedObjects.Add(Spawn(objectToPool, pos, Quaternion.identity));
                } else
                {

                    //Despawn objects at the same rate but with a 1 second delay after everything is spawned.
                    if (spawnedObjects.Count > 0)
                    {
                        Debug.Log("Despawning object to pool");
                        GameObject objectToDespawn = spawnedObjects[spawnedObjects.Count - 1];
                        
                        DeSpawn(objectToDespawn, 0.5f);
                        spawnedObjects.Remove(objectToDespawn);
                    }

                }

                spawnTimer = 0;
            }
        }
        /// <summary>
        /// This is a simple abstraction of the PoolManager.GetObject method. 
        /// </summary>
        /// <param name="objectToSpawn">The object you wnat a new instance of (usually a prefab)</param>
        /// <param name="position">Spawn position</param>
        /// <param name="rotation">Spawn rotation</param>
        public GameObject Spawn(GameObject objectToSpawn, Vector3 position, Quaternion rotation)
        {
            return PoolManager.Instance.GetPooledObject(objectToSpawn, position, rotation);
        }
        /// <summary>
        /// DEspawn function. Pass in the instance of a gameobject NOT the original prefab
        /// </summary>
        /// <param name="objectToDeSpawn">Instance of a game object to despawn</param>
        /// <param name="delay">delay in seconds before the object will deactivate</param>
        public void DeSpawn(GameObject objectToDeSpawn, float delay)
        {
            if (delay > 0)
            {

                StartCoroutine(DelayedDespawn(objectToDeSpawn, delay));
                
            } else
            {
                PoolManager.Instance.DestroyObject(objectToDeSpawn);
            }

        }
        /// <summary>
        /// Simple coroutine to show the delay the despawn
        /// </summary>
        /// <param name="objectToDeSpawn">Object to dispawn</param>
        /// <param name="delay">Delay in seconds</param>
        /// <returns></returns>
        IEnumerator DelayedDespawn(GameObject objectToDeSpawn, float delay)
        {
            Debug.Log("waiting for seconds" + delay);
            yield return new WaitForSeconds(delay);
            Debug.Log("DESTROYING");
            PoolManager.Instance.DestroyObject(objectToDeSpawn);
            yield return null;
        }


    }

}
