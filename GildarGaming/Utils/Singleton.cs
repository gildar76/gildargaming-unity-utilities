using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GildarGaming.Utils
{
    /// <summary>
    /// A singleton monoBehaviour. All SIngletons can inherit from this one.
    /// </summary>
    /// <typeparam name="T">Type of the inheriting class</typeparam>
    abstract public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        private static T instance;
        public static T Instance
        {
            get
            {
                return instance;
            }
        }

        public static bool IsInitialized
        {
            get
            {
                return instance != null;
            }
        }

        protected virtual void Awake()
        {
            if (instance != null)
            {
                Debug.LogErrorFormat("{0} is allready instantiated. You can only have one of each singleton at a time.", GetType().Name);
            }
            else
            {
                instance = (T)this;
            }
            //Debug.Log("Instantiating " + gameObject.name);
        }

        protected virtual void OnDestroy()
        {
            if (instance == this)
            {
                instance = null;
            }
        }
    }
}
