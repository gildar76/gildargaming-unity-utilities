using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GildarGaming.Utils.Scheduling
{
    /// <summary>
    /// Implement this interface to use actions in the ActionScheduler
    /// </summary>
	public interface IAction
	{
        void StartAction();
        void CancelAction();
	}
}
