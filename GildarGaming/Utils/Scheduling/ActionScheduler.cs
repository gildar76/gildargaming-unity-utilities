using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GildarGaming.Utils.Scheduling
{
    /// <summary>
    /// Can be used to start and stop actions
    /// </summary>
	public class ActionScheduler : MonoBehaviour
	{
        [SerializeField]
        IAction currentAction;
        /// <summary>
        /// Starts a new action if that action isn't running.
        /// </summary>
        /// <param name="action">The action to start</param>
        public void StartAction(IAction action)
        {
            if (action != currentAction)
            {
                CancelAction();
                currentAction = action;
                currentAction.StartAction();
            }

            
        }
        /// <summary>
        /// Stops the current action.
        /// </summary>
        public void CancelAction()
        {
            if (currentAction == null) return;
            currentAction.CancelAction();
            //Debug.Log("Cancelling" + currentAction.GetType());
        }
	}
}
